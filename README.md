# mata64-ia

- Base de conhecimento utilizando um cálculo baseado na
lógica de predicados, para um agente que controle o funcionamento de 3 elevadores interligados, em um edifício com 20 andares, que apresentem um funcionamento inteligente.
- Comando para rodar a base de conhecimento
	- swipl file.pl

> [Interpretador – SWI Prolog](http://www.swi-prolog.org/) 

> http://wiki.di.uminho.pt/twiki/pub/Education/LC/0506/prolog61-68.pdf
